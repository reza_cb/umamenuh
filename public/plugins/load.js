$(document).ready( function(){
    // Akses Menu
    $('#nav a[href~="' + location.href + '"]').parents('li').addClass('active');
	// DataTable
    $('#kasir1').DataTable({
    	'info' : false,
    	// 'pagingType' : "simple",
    	'scrollY' : "300px",
    	'scrollCollapes' : true,
    	'paging' : false,
    });
    $('#table').DataTable();
    $('#tagihan').DataTable({
        'ordering' : false,
    });
    // Scroll
    $('#bill').slimScroll({
    	'height' : '150px',
    });
    $('#bill2').slimScroll({
        'height' : '150px',
    });
    // Select2
    $('#pelunasan').select2();
    $('.meja').select2();
    // Summernote
    $('#note').summernote();
    // Datepicker
    $('#produk').datepicker({
      'autoclose' : true,
      'format' : 'yyyy-mm-dd',
    });
    $('#pkotor').datepicker({
      'autoclose' : true,
      'format' : 'yyyy-mm-dd',
    });
    $('#pbersih').datepicker({
      'autoclose' : true,
      'format' : 'yyyy-mm-dd',
    });
});