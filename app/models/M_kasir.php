<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kasir extends CI_Model {

	function code_otomatis(){
        $this->db->select('Right(pesanan.id_order,4) as kode ',false);
        $this->db->order_by('id_order', 'desc');
        $this->db->limit(1);
        $query = $this->db->get('pesanan');
        if($query->num_rows()<>0){
            $data = $query->row();
            $kode = intval($data->kode)+1;
        }else{
            $kode = 1;
        }
        $kodemax = str_pad($kode,4,"0",STR_PAD_LEFT);
        $kodejadi  = "PSN-".$kodemax;
        return $kodejadi;
    }

	function list_produk($table){
		return $this->db->get($table);
	}

	function list_order($table){
		$this->db->select('order_temp.*,produk.*');
		$this->db->join('produk','produk.id_produk=order_temp.id_produk');
		return $this->db->get($table);
	}

	function list_temp(){
		$this->db->select('order_temp.*,produk.*');
		$this->db->join('produk','produk.id_produk=order_temp.id_produk');
		$this->db->where('tipe_produk','mkn');
		return $this->db->get('order_temp');
	}

	function get_price_produk($where,$table){
		$this->db->where($where);
		return $this->db->get($table)->row_array();
	}

	function add_cart($data,$table){
		$this->db->insert($table,$data);
	}

	function trash_stok($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function get_harga_mkn(){
		$this->db->select('subharga');
		// $this->db->from('order_temp');
		$this->db->join('produk','order_temp.id_produk=produk.id_produk');
		$this->db->where('tipe_produk','mkn');
		return $this->db->get('order_temp');
	}

	function get_harga_mkn_detail($order){
		$this->db->select('sub_harga');
		// $this->db->from('order_temp');
		$this->db->join('produk','detail_order.id_produk=produk.id_produk');
		$this->db->where('tipe_produk','mkn');
		$this->db->where('id_order',$order);
		return $this->db->get('detail_order');
	}

	function save_pay($pesanan,$table){
		$this->db->insert($table,$pesanan);
		// $id = $this->db->insert_id();
		// return (isset($id)) ? $id : FALSE;
	}

	function get_temp($table){
		$this->db->select('produk.*,order_temp.*');
		$this->db->join('produk','produk.id_produk=order_temp.id_produk');
		return $this->db->get($table);
	}

	function save_detail($detail,$table){
		$this->db->insert($table,$detail);
		$this->db->truncate('order_temp');
	}

	function list_tagihan($table){
		$this->db->select('pesanan.*,meja.*');
		$this->db->join('meja','meja.id_meja=pesanan.id_meja');
		$this->db->where('status_order','p');
		$this->db->order_by('tgl_order','DESC');
		return $this->db->get($table);
	}

	function get_bill($where,$table){
		$this->db->where($where);
		return $this->db->get($table)->row_array();
	}

	function payment($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function trash_order($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function trash_detail($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function cetak_pelanggan($where,$table){
		$this->db->select('pesanan.*,meja.*');
		$this->db->join('meja','meja.id_meja=pesanan.id_meja');
		$this->db->where($where);
		return $this->db->get($table)->row_array();
	}

	function cetak_kitchen($where,$table){
		$this->db->select('pesanan.*,meja.*');
		// $this->db->join('')
		$this->db->where($where);
		$this->db->get($table);
	}

	function ambil_detail($where,$table){
		$this->db->select('produk.*,detail_order.*');
		$this->db->join('produk','produk.id_produk=detail_order.id_produk');
		$this->db->where($where);
		$this->db->group_by('nama_produk');
		$this->db->select_sum('qty');
		$this->db->select_sum('sub_harga');
		return $this->db->get($table);
	}

	function ambil_detail_guide($where,$table){
		$this->db->select('produk.*,detail_order.*');
		$this->db->join('produk','produk.id_produk=detail_order.id_produk');
		$this->db->where($where);
		$this->db->where('tipe_produk','mkn');
		$this->db->group_by('nama_produk');
		$this->db->select_sum('qty');
		$this->db->select_sum('sub_harga');
		return $this->db->get($table);
	}

	function ambil_detail_guide_new($table){
		$this->db->select('produk.*,order_temp.*');
		$this->db->join('produk','produk.id_produk=order_temp.id_produk');
		$this->db->where('tipe_produk','mkn');
		$this->db->group_by('nama_produk');
		$this->db->select_sum('qty');
		$this->db->select_sum('subharga');
		return $this->db->get($table);
	}

	function ambil_bill($where,$table){
		$this->db->select('pesanan.*,detail_order.*,produk.*,meja.*');
		$this->db->join('meja','meja.id_meja=pesanan.id_meja');
		$this->db->join('detail_order','detail_order.id_order=pesanan.id_order');
		$this->db->join('produk','produk.id_produk=detail_order.id_produk');
		$this->db->where($where);
		$this->db->group_by('nama_produk');
		$this->db->select_sum('qty');
		$this->db->select_sum('sub_harga');
		return $this->db->get($table)->row_array();
	}

	function detail($where1,$table){
		$this->db->select('detail_order.*,produk.*');
		$this->db->join('produk','produk.id_produk=detail_order.id_produk');
		$this->db->where($where1);
		return $this->db->get($table);
	}

	function replace_bill($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function clean_temp($table){
		$this->db->truncate($table);
	}

}

/* End of file M_kasir.php */
/* Location: ./application/models/M_kasir.php */