<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_laporan extends CI_Model {

	function list_sell_item($where,$table){
		$this->db->select('produk.*,detail_order.*,pesanan.*');
		$this->db->join('detail_order','detail_order.id_order=pesanan.id_order');
		$this->db->join('produk','produk.id_produk=detail_order.id_produk');
		$this->db->where($where);
		$this->db->group_by('nama_produk');
		$this->db->select_sum('qty');
		return $this->db->get($table);
	}

	function list_sell_item_bulan($where,$table){
		$this->db->select('produk.*,detail_order.*,pesanan.*');
		$this->db->join('detail_order','detail_order.id_order=pesanan.id_order');
		$this->db->join('produk','produk.id_produk=detail_order.id_produk');
		$this->db->where('month(tgl_order)',$where);
		$this->db->group_by('nama_produk');
		$this->db->select_sum('qty');
		return $this->db->get($table);
	}

	function get_harga($where,$table){
		$this->db->select('sub_harga');
		$this->db->join('detail_order','detail_order.id_order=pesanan.id_order');
		$this->db->where($where);
		return $this->db->get($table);
	}

	function get_harga_bulan($where,$table){
		$this->db->select('sub_harga');
		$this->db->join('detail_order','detail_order.id_order=pesanan.id_order');
		$this->db->where('month(tgl_order)',$where);
		return $this->db->get($table);
	}

	function list_sell_dirty($where,$table){
		$this->db->select('pesanan.*,meja.*');
		$this->db->join('meja','meja.id_meja=pesanan.id_meja');
		$this->db->where($where);
		return $this->db->get($table);
	}

	function list_sell_dirty_bulan($where,$table){
		$this->db->select('pesanan.*,meja.*');
		$this->db->join('meja','meja.id_meja=pesanan.id_meja');
		$this->db->where('month(tgl_order)',$where);
		return $this->db->get($table);
	}

	function get_kotor($where,$table){
		$this->db->select('total_kotor,total_bersih,komisi');
		$this->db->where($where);
		return $this->db->get($table);
	}

	function get_kotor_bulan($where,$table){
		$this->db->select('total_kotor,total_bersih,komisi');
		$this->db->where('month(tgl_order)',$where);
		return $this->db->get($table);
	}

}

/* End of file M_laporan.php */
/* Location: ./application/models/M_laporan.php */