<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_transaksi extends CI_Model {

	function list_transaksi($table){
		return $this->db->get($table);
	}

}

/* End of file M_transaksi.php */
/* Location: ./application/models/M_transaksi.php */