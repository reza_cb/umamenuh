<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_makanan extends CI_Model {

	function list_makanan($table){
		$this->db->where('tipe_produk','mkn');
		return $this->db->get($table);
	}

	function create($data,$table){
		$this->db->insert($table,$data);
	}

	function get($where,$table){
		$this->db->where($where);
		return $this->db->get($table)->row_array();
	}

	function replace($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function trash($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

}

/* End of file M_makanan.php */
/* Location: ./application/models/M_makanan.php */