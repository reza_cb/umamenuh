<div class="content-wrapper">
	<section class="content-header">
		<h1>Edit Data Menu</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<form method="post" action="<?php echo site_url('Admin/Minuman/update') ?>">
							<div class="form-group">
								<label class="control-label">Nama Minuman</label>
								<input type="text" name="minuman" class="form-control" value="<?php echo $produk['nama_produk'] ?>" required>
							</div>
							<div class="form-group">
								<label class="control-label">Detail Produk</label>
								<textarea name="detail" id="note"><?php echo $produk['detail_produk'] ?></textarea>
							</div>
							<div class="form-group">
								<label class="control-label">Harga Produk</label>
								<input type="text" name="harga" class="form-control" value="<?php echo $produk['harga_produk'] ?>" onkeypress=" return hanyaAngka(event)" required>
							</div>
							<div class="form-group">
								<input type="hidden" name="id" class="form-control" value="<?php echo $produk['id_produk'] ?>" required readonly>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	// generate
    function hanyaAngka(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		  	return true;
	}
</script>