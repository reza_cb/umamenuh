<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Menu Makanan</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->flashdata('status') == "berhasil"){ ?>
					<div class="alert alert-success"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
				<div class="box">
					<div class="box-header">
						<a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-success"><i class="fa fa-plus"></i></a>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Nama Produk</th>
									<th class="text-center">Harga</th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($produk as $p){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $p->nama_produk ?></td>
									<td><?php echo number_format($p->harga_produk,'0',',','.') ?></td>
									<td class="text-center">
										<a href="<?php echo site_url('Admin/Makanan/edit/'.$p->id_produk) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
										<a href="#" data-url="<?php echo site_url('Admin/Makanan/hapus/'.$p->id_produk) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Tambah -->
	<div class="modal fade" role="dialog" id="tambah">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Tambah Menu Makanan</h4>
				</div>
				<div class="modal-body">
					<form action="<?php echo site_url('Admin/Makanan/tambah') ?>" method="post">
						<div class="form-group">
							<label class="control-label">Nama  Makanan</label>
							<input type="text" name="makanan" class="form-control" required>
						</div>
						<div class="form-group">
							<label class="control-label">Detail Makanan</label>
							<textarea name="detail" id="note"></textarea>
						</div>
						<div class="form-group">
							<label class="control-label">Harga Makanan</label>
							<input type="text" name="harga" class="form-control" onkeypress=" return hanyaAngka(event)" required>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	// Hapus
    $(document).ready(function(){
      $('.confirm_delete').on('click', function(){
        
        var delete_url = $(this).attr('data-url');

        swal({
          title: "Hapus Produk",
          text: "Yakin ingin menghapus ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#FF111C",
          confirmButtonText: "Hapus !",
          cancelButtonText: "Batalkan",
          closeOnConfirm: false     
        }, function(){
          window.location.href = delete_url;
        });

        return false;
      });
    });
    // generate
    function hanyaAngka(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		  	return true;
	}
</script>