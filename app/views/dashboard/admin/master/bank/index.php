<div class="content-wrapper">
	<section  class="content-header">
		<h1>Daftar Bank</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->flashdata('status') == "berhasil"){ ?>
				<div class="alert alert-success"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
				<div class="box">
					<div class="box-header">
						<a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-success"><i class="fa fa-plus"></i></a>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Kode Bank</th>
									<th class="text-center">Nama Bank</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($bank as $b){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td class="text-center"><?php echo $b->kode_bank ?></td>
									<td><?php echo $b->nama_bank ?></td>
									<td class="text-center">
										<a href="#" data-toggle="modal" data-target="#edit" onclick="edit(<?php echo $b->id_bank ?>)" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
										<a href="#" data-url="<?php echo site_url('Admin/Bank/hapus/'.$b->id_bank) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Tambah -->
		<div class="modal fade" role="dialog" id="tambah">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4>Tambah Data Bank</h4>
						<a href="#" class="close" data-dismiss="modal"><i class="fa fa-times"></i></a>
					</div>
					<div class="modal-body">
						<form method="post" action="<?php echo site_url('Admin/Bank/tambah') ?>">
							<div class="form-group">
								<label class="control-label">Kode Bank</label>
								<input type="text" name="kode" class="form-control" placeholder="Ex: BCA, BNI" autofocus required>
							</div>
							<div class="form-group">
								<label class="control-label">Nama Bank</label>
								<input type="text" name="bank" class="form-control" placeholder="Ex: Bank Central Asia" required>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- Edit -->
		<div class="modal fade" role="dialog" id="edit">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4>Edit Data Bank</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo site_url('Admin/Bank/update') ?>" method="post">
							<div class="form-group">
								<label class="control-label">Kode Bank</label>
								<input type="text" name="kode" class="form-control" id="kode" placeholder="Kode Bank" required autofocus>
							</div>
							<div class="form-group">
								<label class="control-label">Nama Bank</label>
								<input type="text" name="bank" class="form-control" id="nbank" placeholder="Nama Bank" required>
							</div>
							<div class="form-group">
								<input type="hidden" name="id" class="form-control" id="unik" readonly>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	// Hapus
    $(document).ready(function(){
      $('.confirm_delete').on('click', function(){
        
        var delete_url = $(this).attr('data-url');

        swal({
          title: "Hapus Bank",
          text: "Yakin ingin menghapus ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#FF111C",
          confirmButtonText: "Hapus !",
          cancelButtonText: "Batalkan",
          closeOnConfirm: false     
        }, function(){
          window.location.href = delete_url;
        });

        return false;
      });
    });
	//ajax script edit

    function edit(idbank){
    	$.ajax({
            url:"<?php echo site_url('Admin/Bank/edit');?>",
            type:"post",
            dataType: 'json',
            data:{id:idbank},
            cache:false,
            success:function(result){
              $('#unik').val(result['id_bank']);
              $('#kode').val(result['kode_bank']);
              $('#nbank').val(result['nama_bank']);
            }
        });
    }
</script>