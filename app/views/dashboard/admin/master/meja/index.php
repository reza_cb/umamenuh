<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Meja</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->flashdata('status') == "berhasil"){ ?>
					<div class="alert alert-success"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
				<div class="box">
					<div class="box-header">
						<a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-success"><i class="fa fa-plus"></i></a>
					</div>
					<div class="box-header">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Nama Meja</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($meja as $m){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $m->nama_meja ?></td>
									<td class="text-center">
										<a href="#" data-toggle="modal" data-target="#edit" onclick="edit(<?php echo $m->id_meja ?>)" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
										<a href="#" data-url="<?php echo site_url('Admin/Meja/hapus/'.$m->id_meja) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" role="dialog" id="tambah">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Tambah Data Meja</h4>
				</div>
				<div class="modal-body">
					<form action="<?php echo site_url('Admin/Meja/tambah') ?>" method="post">
						<div class="form-group">
							<input type="text" name="meja" class="form-control" placeholder="Nama Meja" required>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Edit -->
	<div class="modal fade" role="dialog" id="edit">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Edit Data Meja</h4>
				</div>
				<div class="modal-body">
					<form action="<?php echo site_url('Admin/Meja/update') ?>" method="post">
						<div class="form-group">
							<input type="text" name="meja" class="form-control" id="nama" placeholder="Nama Meja" required>
						</div>
						<div class="form-group">
							<input type="hidden" name="id" class="form-control" id="unik" readonly>
							<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	// Hapus
    $(document).ready(function(){
      $('.confirm_delete').on('click', function(){
        
        var delete_url = $(this).attr('data-url');

        swal({
          title: "Hapus Meja",
          text: "Yakin ingin menghapus ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#FF111C",
          confirmButtonText: "Hapus !",
          cancelButtonText: "Batalkan",
          closeOnConfirm: false     
        }, function(){
          window.location.href = delete_url;
        });

        return false;
      });
    });
	//ajax script edit

    function edit(idmeja){
    	$.ajax({
            url:"<?php echo site_url('Admin/Meja/edit');?>",
            type:"post",
            dataType: 'json',
            data:{id:idmeja},
            cache:false,
            success:function(result){
              $('#unik').val(result['id_meja']);
              $('#nama').val(result['nama_meja']);
            }
        });
    }
</script>