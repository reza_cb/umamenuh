<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Transaksi</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">No. Pesanan</th>
									<th class="text-center">Tgl Pesanan</th>
									<th class="text-center">Total</th>
									<th class="text-center">Status</th>
									<!-- <th class="text-center">Action</th> -->
								</tr>
							</thead>
							<tbody>
								<?php foreach($transaksi as $t){ ?>
								<tr>
									<td class="text-center"><?php echo $t->id_order ?></td>
									<td><?php echo date('d F Y ',strtotime($t->tgl_order)) ?></td>
									<td>Rp <?php echo number_format($t->total_kotor,'0',',','.') ?></td>
									<td class="text-center">
										<?php 
											if($t->status_order == "l"){
												echo "Lunas";
											}else{
												echo "Belum bayar";
											}
										?>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>