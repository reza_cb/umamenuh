<div class="content-wrapper">
	<section class="content-header">
		<h1>Menu Laporan</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<div class="row">
							<div class="col-md-6 text-center">
								<i class="fa fa-file-pdf-o fa-4x"></i>
								<p>Laporan Penjualan Item (Harian)</p>
								<a href="#" data-toggle="modal" data-target="#item" class="btn btn-primary"><i class="fa fa-print"></i> Cetak</a>
							</div>
							<div class="col-md-6 text-center">
								<i class="fa fa-file-pdf-o fa-4x"></i>
								<p>Laporan Pendapatan (Harian)</p>
								<a href="#" data-toggle="modal" data-target="#kotor" class="btn btn-primary"><i class="fa fa-print"></i> Cetak</a>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6 text-center">
								<i class="fa fa-file-pdf-o fa-4x"></i>
								<p>Laporan Penjualan Item (Bulanan)</p>
								<a href="#" data-toggle="modal" data-target="#itembulan" class="btn btn-primary"><i class="fa fa-print"></i> Cetak</a>
							</div>
							<div class="col-md-6 text-center">
								<i class="fa fa-file-pdf-o fa-4x"></i>
								<p>Laporan Pendapatan (Bulanan)</p>
								<a href="#" data-toggle="modal" data-target="#kotorbulan" class="btn btn-primary"><i class="fa fa-print"></i> Cetak</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" role="dialog" id="item">
		<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Cetak Laporan Penjualan Per Item (Harian)</h4>
			</div>
			<div class="modal-body">
				<form action="<?php echo site_url('Admin/Laporan/cetak_jual_item') ?>" method="post" target="_blank">
					<div class="form-group">
						<input type="text" name="tanggal" class="form-control" id="produk">
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-info"><i class="fa fa-print"></i> Cetak</button>
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>
	<div class="modal fade" role="dialog" id="kotor">
		<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Cetak Laporan Penjualan (Harian)</h4>
			</div>
			<div class="modal-body">
				<form action="<?php echo site_url('Admin/Laporan/cetak_jual_kotor') ?>" method="post" target="_blank">
					<div class="form-group">
						<input type="text" name="tanggal" class="form-control" id="pkotor">
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-info"><i class="fa fa-print"></i> Cetak</button>
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>
	<div class="modal fade" role="dialog" id="itembulan">
		<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Cetak Laporan Penjualan Per Item (Bulanan)</h4>
			</div>
			<div class="modal-body">
				<form action="<?php echo site_url('Admin/Laporan/cetak_jual_item_bulan') ?>" method="post" target="_blank">
					<div class="form-group">
						<select name="bulan" class="form-control">
							<option>-- Pilih Bulan --</option>
							<option value="01">Januari</option>
							<option value="02">Februari</option>
							<option value="03">Maret</option>
							<option value="04">April</option>
							<option value="05">Mei</option>
							<option value="06">Juni</option>
							<option value="07">Julii</option>
							<option value="08">Agustus</option>
							<option value="09">September</option>
							<option value="10">Oktober</option>
							<option value="11">November</option>
							<option value="12">Desember</option>
						</select>
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-info"><i class="fa fa-print"></i> Cetak</button>
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>
	<div class="modal fade" role="dialog" id="kotorbulan">
		<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Cetak Laporan Penjualan (Bulanan)</h4>
			</div>
			<div class="modal-body">
				<form action="<?php echo site_url('Admin/Laporan/cetak_jual_kotor_bulan') ?>" method="post" target="_blank">
					<div class="form-group">
						<select name="bulan" class="form-control">
							<option>-- Pilih Bulan --</option>
							<option value="01">Januari</option>
							<option value="02">Februari</option>
							<option value="03">Maret</option>
							<option value="04">April</option>
							<option value="05">Mei</option>
							<option value="06">Juni</option>
							<option value="07">Julii</option>
							<option value="08">Agustus</option>
							<option value="09">September</option>
							<option value="10">Oktober</option>
							<option value="11">November</option>
							<option value="12">Desember</option>
						</select>
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-info"><i class="fa fa-print"></i> Cetak</button>
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>
</div>