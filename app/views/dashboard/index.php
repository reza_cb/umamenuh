<?php if($this->session->userdata('akses') == "admin"){ ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>
              <?php 
                $this->db->from('pesanan');
                echo $this->db->count_all_results();
              ?>
            </h3>
            <p>Total Transaksi</p>
          </div>
          <div class="icon">
            <i class="fa fa-shopping-cart"></i>
          </div>
          <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>
              <?php 
                $this->db->from('pesanan');
                $this->db->where('tgl_order',date('Y-m-d'));
                echo $this->db->count_all_results();
              ?>
            </h3>
            <p>Transaksi Hari Ini</p>
          </div>
          <div class="icon">
            <i class="fa fa-shopping-cart"></i>
          </div>
          <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php } ?>
<?php if($this->session->userdata('akses') == "kasir"){ ?>
<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-md-8">
        <div class="box">
          <div class="box-body">
            <table class="table table-bordered table-striped" id="kasir1">
              <thead>
                <tr>
                  <th class="text-center">Menu</th>
                  <th class="text-center">Jumlah</th>
                  <th class="text-center">Catatan dapur</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($produk as $p){ ?>
                <tr>
                  <td><?php echo $p->nama_produk ?></td>
                  <form method="post" action="<?php echo site_url('Kasir/tambah_keranjang') ?>">
                    <td>
                      <input type="tex" name="qty" class="form-control" required>
                    </td>
                    <td>
                      <input type="text" name="catatan" class="form-control">
                    </td>
                    <td>
                      <input type="hidden" name="produk" class="form-control" value="<?php echo $p->id_produk ?>" readonly>
                      <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-cart-plus"></i></button>
                    </td>
                  </form>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box">
          <div class="box-header">
            <h5 class="text-center">Warung Uma Menuh</h5>
            <h5 class="text-center">Catatan Pembelian</h5>
          </div>
          <form method="post" action="<?php echo site_url('Kasir/simpan_tagihan') ?>">
          <div class="box-body" id="bill">
            <table class="table">
              <?php foreach($order as $o){ ?>
              <tr>
                <td>
                  <?php echo $o->nama_produk ?>
                  <br><label class="control-label"><?php echo $o->catatan ?></label>    
                </td>
                <td style="width: 30%">
                  <input type="text" name="subharga" class="form-control" value="<?php echo $o->subharga ?>" id="subharga<?php echo $o->id_produk ?>" readonly>
                  <input type="hidden" name="" class="form-control" value="<?php echo $o->harga_produk ?>" id="harga<?php echo $o->id_produk ?>" readonly>`
                </td>
                <td style="width: 15%">
                  <input type="text" name="qty" class="form-control" value="<?php echo $o->qty ?>" id="qty<?php echo $o->id_produk ?>" onchange="subtotal<?php echo $o->id_produk ?>()" readonly>
                </td>
                <td>
                  <a href="<?php echo site_url('Kasir/delete_stok/'.$o->id_produk) ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <?php } ?>
            </table>
          </div>
          <div class="box-footer">
            <div class="form-group">
              <input type="hidden" name="order" class="form-control" value="<?php echo $kode ?>" readonly>
            </div>
            <div class="form-group">
              <div class="col-md-6">
                <select class="meja" name="meja" style="width: 100%">
                  <option>-- Pilih Meja --</option>
                  <?php foreach($meja as $m){ ?>
                  <option value="<?php echo $m->id_meja ?>"><?php echo $m->nama_meja ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-6">
                <input type="text" name="kode" class="form-control" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Total :</label>
              <input type="text" name="kotor" value="" id="kotor" onclick="total()" class="form-control" readonly>
              <input type="hidden" name="bersih" value="" id="bersih" class="form-control" readonly>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript">
  <?php foreach($order as $o){ ?>
  function subtotal<?php echo $o->id_produk ?>(){
    var jumlah = document.getElementById('qty<?php echo $o->id_produk ?>').value;
    var harga = document.getElementById('harga<?php echo $o->id_produk ?>').value;
    var sub = parseInt(jumlah) * parseInt(harga);

    document.getElementById('subharga<?php echo $o->id_produk ?>').value = sub;
  }
  <?php } ?>
  function total(){
    var tax = 0.11;
    var service = 0.1;
    var arr = document.getElementsByName("subharga"); 
    var tot=0;

    for(var i=0;i<arr.length;i++){
      if(parseInt(arr[i].value)){
        tot += parseInt(arr[i].value);
      }
    }

    var subtax = tot * tax;
    var subservice = tot * service;
    document.getElementById('kotor').value = tot + subtax + subservice;
    document.getElementById('bersih').value = tot;
  }
</script>
<?php } ?>