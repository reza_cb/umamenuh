<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0.1
  </div>
  <strong>Copyright &copy; Cebong Solution <?php echo date('Y') ?>.
</footer>
</div>
<!-- ./wrapper -->
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url() ?>public/dashboard/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>

<!-- jQuery Knob Chart -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/dashboard/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>public/dashboard/dist/js/adminlte.min.js"></script>
<!-- DataTable -->
<script src="<?php echo base_url() ?>public/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>public/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>public/plugins/select2/dist/js/select2.min.js"></script>
<!-- Sweetalert -->
<script src="<?php echo base_url() ?>public/plugins/sweetalert/dist/sweetalert.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url() ?>public/plugins/summernote/dist/summernote.min.js"></script>
<!-- Load Plugins -->
<script src="<?php echo base_url() ?>public/plugins/load.js"></script>
</body>
</html>