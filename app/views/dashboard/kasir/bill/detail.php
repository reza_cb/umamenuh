<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<form method="post" action="<?php echo site_url('Kasir/Lunas') ?>">
							<div class="form-group">
								<label class="control-label">Total :</label>
								<input type="text" name="total" class="form-control" value="<?php echo $detail['total_kotor'] ?>" id="total" readonly>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<label class="control-label">Bayar Kartu Debit :</label>
									<select class="form-control" name="kdebit">
										<option>-- Pilih Provider Kartu Debit</option>
										<?php foreach($bank as $b){ ?>
										<option value="<?php echo $b->id_bank ?>"><?php echo $b->nama_bank ?></option>
										<?php } ?>
									</select>
									<input type="text" name="jdebit" class="form-control" value="" id="bayard" onkeyup="pelunasan()">
								</div>
								<div class="col-md-6">
									<label class="control-label">Bayar Kartu Kredit :</label>
									<select class="form-control" name="kkredit">
										<option>-- Pilih Provider Kartu Kredit</option>
										<?php foreach($bank as $b){ ?>
										<option value="<?php echo $b->id_bank ?>"><?php echo $b->nama_bank ?></option>
										<?php } ?>
									</select>
									<input type="text" name="jkredit" class="form-control" value="" id="bayark" onkeyup="pelunasan()">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Bayar Cash :</label>
								<input type="text" name="bayar" class="form-control" value="" id="bayar" onkeyup="pelunasan()">
							</div>
							<div class="form-group">
								<label class="control-label">Kembalian :</label>
								<input type="text" name="kembalian" class="form-control" value="" id="kembalian" readonly>
							</div>
							<div class="form-group">
								<input type="hidden" name="id" class="form-control" value="<?php echo $detail['id_order'] ?>" readonly>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	function pelunasan(){
		var total = document.getElementById('total').value;
		var bayar = document.getElementById('bayar').value;
		var debit = document.getElementById('bayard').value;
		var kredit = document.getElementById('bayark').value;

		if(debit != 0){
			var hitung = debit - total;
			document.getElementById('kembalian').value = hitung;
		}else if(kredit != 0){
			var hitung = kredit - total;
			document.getElementById('kembalian').value = hitung;
		}else{
			var hitung = bayar - total;
			document.getElementById('kembalian').value = hitung;
		}
	}
</script>