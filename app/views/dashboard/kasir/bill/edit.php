<div class="content-wrapper">
	<section class="content-header">
		<h1>Edit Bill</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->flashdata('status') == "berhasil"){ ?>
					<div class="alert alert-success"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered table-striped" id="kasir1">
							<thead>
								<tr>
									<th class="text-center">Nama Produk</th>
									<th class="text-center">Jumlah</th>
									<td class="text-center">Catatan</td>
									<th class="text-center">#</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($produk as $p){ ?>
								<tr>
									<td><?php echo $p->nama_produk ?></td>
									<form method="post" action="<?php echo site_url('Kasir/update_detail') ?>">
									<td>
										<input type="text" name="qty" class="form-control" required>
										<input type="hidden" name="produk" class="form-control" value="<?php echo $p->id_produk ?>" readonly>
									</td>
									<td>
										<input type="text" name="catatan" class="form-control">
									</td>
									<td>
										<input type="hidden" name="order" class="form-control" value="<?php echo $bill['id_order'] ?>" readonly>
										<button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-cart-plus"></i></button>
									</td>
									</form>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box">
					<div class="box-body" id="bill">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">Menu</th>
									<th class="text-center">Sub harga</th>
									<th class="text-center">Qty</th>
									<th class="text-center">#</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($detail as $d){ ?>
								<tr>
									<form action="<?php echo site_url('Kasir/hapus_detail') ?>" method="post">
									<td>
										<?php echo $d->nama_produk ?>
										<br><label class="control-label"><?php echo $d->catatan ?></label>		
									</td>
									<td>
										<input type="text" name="subharga" class="form-control" id="subharga<?php echo $d->id_produk ?>" value="<?php echo $d->sub_harga ?>" readonly>
										<input type="hidden" name="harga" class="form-control" id="harga<?php echo $d->id_produk ?>" value="<?php echo $d->harga_produk ?>">
									</td>
									<td><input type="number" class="form-control" name="qty" id="qty<?php echo $d->id_produk ?>" value="<?php echo $d->qty ?>" onchange="subtotal<?php echo $d->id_produk ?>()" readonly></td>
									<td>
										<input type="hidden" name="produk" class="form-control" value="<?php echo $d->id_produk ?>" readonly>
										<input type="hidden" name="order" class="form-control" value="<?php echo $d->id_order ?>" readonly>
										<button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
										<!-- <a href="<?php echo site_url('Kasir/hapus_detail') ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a> -->
									</td>
									</form>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<form method="post" action="<?php echo site_url('Kasir/update_bill') ?>">
					<div class="box-footer">
						<div class="form-group">
			              <input type="hidden" name="order" class="form-control" value="<?php echo $bill['id_order'] ?>" readonly>
			            </div>
			            <div class="form-group">
			              <div class="col-md-6">
			                <select class="meja" name="meja" style="width: 100%">
			                  <option>-- Pilih Meja --</option>
			                  <?php foreach($meja as $m){ ?>
			                  <option value="<?php echo $m->id_meja ?>" <?php if($bill['id_meja'] == $m->id_meja){echo "selected";} ?>><?php echo $m->nama_meja ?></option>
			                  <?php } ?>
			                </select>
			              </div>
			              <div class="col-md-6">
			                <input type="text" name="kode" class="form-control" value="<?php echo $bill['kodeunik'] ?>" required>
			              </div>
			            </div>
			            <div class="form-group">
			              <label class="control-label">Total :</label>
			              <input type="text" name="kotor" value="" id="kotor" value="<?php echo $bill['total_kotor'] ?>" onclick="total()" class="form-control" readonly>
			              <input type="hidden" name="bersih" value="" id="bersih" value="<?php echo $bill['total_bersih'] ?>" class="form-control" readonly>
			            </div>
			            <div class="form-group">
			                <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
			            </div>
					</div>
					</form>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box">
					<form method="post" action="<?php echo site_url('Kasir/cetak_baru') ?>">
					<div class="box-body" id="bill2">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">Menu</th>
									<th class="text-center">Harga</th>
									<th class="text-center">QTY</th>
									<th class="text-center">Catatan</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($temp as $t){ ?>
								<tr>
									<td><?php echo $t->nama_produk ?></td>
									<td>Rp <?php echo $t->subharga ?></td>
									<td><?php echo $t->qty ?></td>
									<td><?php echo $t->catatan ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<div class="box-footer">
						<input type="hidden" name="order" class="form-control" value="<?php echo $bill['id_order'] ?>" readonly>
						<button type="submit" class="btn btn-success"><i class="fa fa-print"></i></button>
						<a href="<?php echo site_url('Kasir/simpan_baru/'.$bill['id_order']) ?>" class="btn btn-info"><i class="fa fa-save"></i></a>
						<a href="<?php echo site_url('Kasir/bersih_temp/'.$bill['id_order']) ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
					</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
  <?php foreach($detail as $d){ ?>
  function subtotal<?php echo $d->id_produk ?>(){
    var jumlah = document.getElementById('qty<?php echo $d->id_produk ?>').value;
    var harga = document.getElementById('harga<?php echo $d->id_produk ?>').value;
    var sub = parseInt(jumlah) * parseInt(harga);

    document.getElementById('subharga<?php echo $d->id_produk ?>').value = sub;
  }
  <?php } ?>
  function total(){
    var tax = 0.11;
    var service = 0.1;
    var arr = document.getElementsByName("subharga"); 
    var tot=0;

    for(var i=0;i<arr.length;i++){
      if(parseInt(arr[i].value)){
        tot += parseInt(arr[i].value);
      }
    }

    var subtax = tot * tax;
    var subservice = tot * service;
    document.getElementById('kotor').value = tot + subtax + subservice;
    document.getElementById('bersih').value = tot;
  }
</script>