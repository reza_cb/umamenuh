<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Bill</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered table-striped" id="tagihan">
							<thead>
								<tr>
									<th class="text-center">No. Pesanan</th>
									<th class="text-center">No. Meja</th>
									<th class="text-center">Total Pembayaran</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($tagihan as $t){ ?>
								<tr>
									<td class="text-center"><?php echo $t->id_order ?></td>
									<td class="text-center"><?php echo $t->nama_meja ?></td>
									<td>Rp <?php echo number_format($t->total_kotor,'0',',','.') ?></td>
									<td class="text-center">
										<a href="<?php echo site_url('Dashboard/bill_detail/'.$t->id_order) ?>" class="btn btn-info btn-xs" title="Bayar"><i class="fa fa-credit-card"></i></a>
										<a href="<?php echo site_url('Kasir/cetak_guide/'.$t->id_order) ?>" class="btn btn-warning btn-xs" title="Cetak Guide"><i class="fa fa-print"></i></a>
										<a href="<?php echo site_url('Kasir/cetak_dapur/'.$t->id_order) ?>" class="btn btn-deafult btn-xs" title="Cetak Kitchen"><i class="fa fa-flag-o"></i></a>
										<a href="<?php echo site_url('Kasir/cetak_bill/'.$t->id_order) ?>" class="btn btn-primary btn-xs" title="Cetak Bill"><i class="fa fa-print"></i></a>
										<a href="<?php echo site_url('Kasir/edit_bill/'.$t->id_order) ?>" class="btn btn-success btn-xs" title="Edit"><i class="fa fa-edit"></i></a>
										<a href="<?php echo site_url('Kasir/hapus_pesanan/'.$t->id_order) ?>" class="btn btn-danger btn-xs" title="Hapus Pesanan"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>