<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kasir extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_kasir');
		$this->load->model('M_meja');
	}

	public function tambah_keranjang(){
		$where = array('id_produk' => $this->input->post('produk'));
		$harga = $this->M_kasir->get_price_produk($where,'produk');
		$subharga = $this->input->post('qty') *  $harga['harga_produk'];

		$data = array(
			'id_produk' => $this->input->post('produk'),
			'qty' => $this->input->post('qty'),
			'catatan' => $this->input->post('catatan'),
			'subharga' => $subharga,
		);

		$this->M_kasir->add_cart($data,'order_temp');
		redirect('Dashboard');
	}

	function delete_stok($id){
		$where = array('id_produk' => $id);
		$this->M_kasir->trash_stok($where,'order_temp');
		redirect('Dashboard');
	}

	function simpan_tagihan(){
		$order = $this->input->post('order');
		$meja = $this->input->post('meja');
		$kodeunik = $this->input->post('kode');
		$bersih = $this->input->post('bersih');
		$kotor = $this->input->post('kotor');
		$tgl = date('Y-m-d');

		$cek = $this->M_kasir->list_temp();
		$komisi = $this->M_kasir->get_harga_mkn()->result();

		if($cek->num_rows() > 0){
			foreach($komisi as $k){
				$harga[] = $k->subharga;
			}

			$totalmkn = array_sum($harga);
			$hitungkomisi = $totalmkn * 0.35;

			$pesanan = array(
				'id_order' => $order,
				'id_meja' => $meja,
				'total_kotor' => $kotor,
				'total_bersih' => $bersih,
				'kodeunik' => $kodeunik,
				'komisi' => $hitungkomisi,
				'tgl_order' => $tgl,
				'status_order' => "p",
			);

			$this->M_kasir->save_pay($pesanan,'pesanan');
			$get = $this->M_kasir->get_temp('order_temp')->result();
			
			foreach($get as $g){
				$p = $g->id_produk;
				$q = $g->qty;
				$o = $g->catatan;
				$h = $g->qty * $g->harga_produk;

				$detail = array(
					'id_order' => $order,
					'id_produk' => $p,
					'qty' => $q,
					'catatan' => $o,
					'sub_harga' => $h,
				);
				// var_dump($data);
				$this->M_kasir->save_detail($detail,'detail_order');
			}
			redirect('Dashboard');
		}else{
			$pesanan = array(
				'id_order' => $order,
				'id_meja' => $meja,
				'total_kotor' => $kotor,
				'total_bersih' => $bersih,
				'kodeunik' => $kodeunik,
				'komisi' => 0,
				'tgl_order' => $tgl,
				'status_order' => "p",
			);

			$this->M_kasir->save_pay($pesanan,'pesanan');
			$get = $this->M_kasir->get_temp('order_temp')->result();
			
			foreach($get as $g){
				$p = $g->id_produk;
				$q = $g->qty;
				$o = $g->catatan;
				$h = $g->qty * $g->harga_produk;

				$detail = array(
					'id_order' => $order,
					'id_produk' => $p,
					'qty' => $q,
					'catatan' => $o,
					'sub_harga' => $h,
				);
				// var_dump($data);
				$this->M_kasir->save_detail($detail,'detail_order');
			}
			redirect('Dashboard');
		}
	}

	function hapus_pesanan($id){
		$where = array('id_order' => $id);

		$this->M_kasir->trash_order($where,'pesanan');
		$this->M_kasir->trash_detail($where,'detail_order');

		redirect('Dashboard/bill');
	}

	function hapus_detail(){
		$where = array(
			'id_order' => $this->input->post('order'),
			'id_produk' => $this->input->post('produk'),
		);

		$this->M_kasir->trash_detail($where,'detail_order');
		redirect('Kasir/edit_bill/'.$this->input->post('order'));
	}

	function lunas(){
		$where = array('pesanan.id_order' => $this->input->post('id'));
		$cash = $this->input->post('bayar');
		$debit = $this->input->post('jdebit');
		$kredit = $this->input->post('jkredit');

		if($cash != null){
			$data = array(
				'bayar' => $cash,
				'kembalian' => $this->input->post('kembalian'),
				'status_order' => "l",
			);

			// var_dump($data);
			$this->M_kasir->payment($where,$data,'pesanan');
			redirect('Dashboard/bill_detail/'.$this->input->post('id'),'refresh');
		}else if($debit != null){
			$data = array(
				'id_bank' => $this->input->post('kdebit'),
				'bayar_debit' => $debit,
				'kembalian' => $this->input->post('kembalian'),
				'status_order' => "l",
			);

			// var_dump($data);
			$this->M_kasir->payment($where,$data,'pesanan');
			redirect('Dashboard/bill_detail/'.$this->input->post('id'),'refresh');
		}else{
			$data = array(
				'id_bank' => $this->input->post('kkredit'),
				'bayar_kredit' => $kredit,
				'kembalian' => $this->input->post('kembalian'),
				'status_order' => "l",
			);

			// var_dump($data);
			$this->M_kasir->payment($where,$data,'pesanan');
			redirect('Dashboard/bill_detail/'.$this->input->post('id'),'refresh');
		}

		// $bill = $this->M_kasir->cetak_pelanggan($where,'pesanan');
		// try {
	 //        $this->load->library('Escpos');
	 //        // Nama Printer
	 //        $this->escpos->connect('MINIPOS');
	 //        $this->escpos->print_test_receipt($bill);
	 //        redirect('Dashboard/bill_detail/'.$this->input->post('id'));
  //       } catch (Exception $e) {
	 //        log_message("error", "Error: Could not print. Message ".$e->getMessage());
	 //        $this->escpos->close_after_exception();
  //       }
	}

	public function cetak_guide($id)
  	{ 
  		$where = array('id_order' => $id);
  		$bill = $this->M_kasir->cetak_pelanggan($where,'pesanan');
  		// print_r($bill);
		try {
	        $this->load->library('Escpos');
	        // Nama Printer
	        $this->escpos->connect('MINIPOS');
	        $this->escpos->print_test_guide($bill);
	        redirect('Dashboard/bill');
        } catch (Exception $e) {
	        log_message("error", "Error: Could not print. Message ".$e->getMessage());
	        $this->escpos->close_after_exception();
        }
 	}

 	public function cetak_dapur($id){
 		$where = array('id_order' => $id);
 		$bill = $this->M_kasir->cetak_pelanggan($where,'pesanan');

 		try{
 			$this->load->library('Escpos');
 			$this->escpos->connect('MINIPOS');
 			$this->escpos->print_kitchen($bill);
 			redirect('Dashboard/bill');
 		} catch (Exception $e){
 			log_message("error", "Error: Could not print. Message ".$e->getMessage());
	        $this->escpos->close_after_exception();
 		}
 	}

 	public function cetak_bill($id){
 		$where = array('id_order' => $id);
  		$bill = $this->M_kasir->cetak_pelanggan($where,'pesanan');
  		// print_r($bill);
		try {
	        $this->load->library('Escpos');
	        // Nama Printer
	        $this->escpos->connect('MINIPOS');
	        $this->escpos->print_test_bill($bill);
	        redirect('Dashboard/bill');
        } catch (Exception $e) {
	        log_message("error", "Error: Could not print. Message ".$e->getMessage());
	        $this->escpos->close_after_exception();
        }
 	}

 	function edit_bill($id){
 		$where = array('pesanan.id_order' => $id);
 		$where1 = array('id_order' => $id);
 		$data['bill'] = $this->M_kasir->ambil_bill($where,'pesanan');
 		$data['detail'] = $this->M_kasir->detail($where1,'detail_order')->result();
 		$data['meja'] = $this->M_meja->list_meja('meja')->result();
 		$data['produk'] = $this->M_kasir->list_produk('produk')->result();
 		$data['temp'] = $this->M_kasir->list_order('order_temp')->result();

 		$this->load->view('dashboard/sidebar');
 		$this->load->view('dashboard/kasir/bill/edit',$data);
 		$this->load->view('dashboard/footer');
 	}

 	function update_detail(){
 		$where = array('id_produk' => $this->input->post('produk'));
		$harga = $this->M_kasir->get_price_produk($where,'produk');
		$subharga = $this->input->post('qty') *  $harga['harga_produk'];

		$data = array(
			'id_produk' => $this->input->post('produk'),
			'qty' => $this->input->post('qty'),
			'catatan' => $this->input->post('catatan'),
			'subharga' => $subharga,
		);

		$this->M_kasir->add_cart($data,'order_temp');
		redirect('Kasir/edit_bill/'.$this->input->post('order'));
 	}

 	function update_bill(){
 		$order = $this->input->post('order');
 		$kotor = $this->input->post('kotor');
 		$bersih = $this->input->post('bersih');

 		$komisi = $this->M_kasir->get_harga_mkn_detail($order)->result();

		foreach($komisi as $k){
			$harga[] = $k->sub_harga;
		}

		$totalmkn = array_sum($harga);
		$hitungkomisi = $totalmkn * 0.35;

		$where = array('id_order' => $order);
		$data = array(
			'total_kotor' => $kotor,
			'total_bersih' => $bersih,
			'komisi' => $hitungkomisi,
		);

		$this->M_kasir->replace_bill($where,$data,'pesanan');
		redirect('Kasir/edit_bill/'.$order);
 	}

 	function cetak_baru(){
 		$order = $this->input->post('order');
 		$where = array('id_order' => $order);
  		$bill = $this->M_kasir->cetak_pelanggan($where,'pesanan');
  		// print_r($bill);
		try {
	        $this->load->library('Escpos');
	        // Nama Printer
	        $this->escpos->connect('MINIPOS');
	        $this->escpos->print_test_guide_new($bill);
	        redirect('Kasir/edit_bill/'.$order);
        } catch (Exception $e) {
	        log_message("error", "Error: Could not print. Message ".$e->getMessage());
	        $this->escpos->close_after_exception();
        }
 	}

 	function simpan_baru($id){
 		$order = $id;
 		$get = $this->M_kasir->get_temp('order_temp')->result();
 		$notif = array(
 			'status' => "berhasil",
 			'message' => "Data pesanan berhasil di update",
 		);
		
		foreach($get as $g){
			$p = $g->id_produk;
			$q = $g->qty;
			$o = $g->catatan;
			$h = $g->qty * $g->harga_produk;

			$detail = array(
				'id_order' => $order,
				'id_produk' => $p,
				'qty' => $q,
				'catatan' => $o,
				'sub_harga' => $h,
			);
			// var_dump($data);
			$this->M_kasir->save_detail($detail,'detail_order');
		}
		// print_r($detail);
		$this->session->set_flashdata($notif);
		redirect('Kasir/edit_bill/'.$order);
 	}

 	function bersih_temp($id){
 		$order = $id;
 		$this->M_kasir->clean_temp('order_temp');
 		redirect('Kasir/edit_bill/'.$order);
 	}

}

/* End of file Kasir.php */
/* Location: ./application/controllers/Kasir.php */