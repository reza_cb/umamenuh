<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profil extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/profil');
		$this->load->view('dashboard/footer');		
	}

}

/* End of file Profil.php */
/* Location: ./application/controllers/Profil.php */