<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Makanan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('M_makanan');
	}

	public function index()
	{
		$data['produk'] = $this->M_makanan->list_makanan('produk')->result();

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/produk/makanan/index',$data);
		$this->load->view('dashboard/footer');
	}

	function tambah(){
		$data = array(
			'nama_produk' => $this->input->post('makanan'),
			'detail_produk' => $this->input->post('detail'),
			'harga_produk' => $this->input->post('harga'),
			'tipe_produk' => "mkn",
		);
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil ditambahkan",
		);

		$this->M_makanan->create($data,'produk');
		$this->session->set_flashdata($notif);
		redirect('Admin/Makanan');
	}

	function edit($id){
		$where = array('id_produk' => $id);
		$data['produk'] = $this->M_makanan->get($where,'produk');

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/produk/makanan/edit',$data);
		$this->load->view('dashboard/footer');
	}

	function update(){
		$where = array('id_produk' => $this->input->post('id'));
		$data = array(
			'nama_produk' => $this->input->post('makanan'),
			'detail_produk' => $this->input->post('detail'),
			'harga_produk' => $this->input->post('harga'),
		);
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil diupdate",
		);

		$this->M_makanan->replace($where,$data,'produk');
		$this->session->set_flashdata($notif);
		redirect('Admin/Makanan');
	}

	function hapus($id){
		$where = array('id_produk' => $id);

		$this->M_makanan->trash($where,'produk');
		redirect('Admin/Makanan');
	}

}

/* End of file Makanan.php */
/* Location: ./application/controllers/Makanan.php */