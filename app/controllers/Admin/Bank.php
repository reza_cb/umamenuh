<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bank extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_bank');
	}

	public function index()
	{
		$data['bank'] = $this->M_bank->list_bank('bank')->result();
		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/master/bank/index',$data);
		$this->load->view('dashboard/footer');
	}

	public function tambah(){
		$data = array(
			'kode_bank' => $this->input->post('kode'),
			'nama_bank' => $this->input->post('bank'),
		);
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil ditambahkan",
		);

		$this->M_bank->create($data,'bank');
		$this->session->set_flashdata($notif);
		redirect('Admin/Bank');
	}

	function edit(){
		$id = $this->input->post('id');
		$result = $this->M_bank->get($id,'bank');

		echo json_encode($result);
	}

	function update(){
		$where = array('id_bank' => $this->input->post('id'));
		$data = array(
			'kode_bank' => $this->input->post('kode'),
			'nama_bank' => $this->input->post('bank'),
		);
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil diperbarui",
		);

		$this->M_bank->replace($where,$data,'bank');
		$this->session->set_flashdata($notif);
		redirect('Admin/Bank');
	}

	function hapus($id){
		$where = array('id_bank' => $id);
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil dihapus",
		);

		$this->M_bank->trash($where,'bank');
		$this->session->set_flashdata($notif);
		redirect('Admin/Bank');
	}

}

/* End of file Bank.php */
/* Location: ./application/controllers/Bank.php */