<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_transaksi');
	}

	public function index()
	{
		$data['transaksi'] = $this->M_transaksi->list_transaksi('pesanan')->result();

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/transaksi/index',$data);
		$this->load->view('dashboard/footer');
	}

}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */