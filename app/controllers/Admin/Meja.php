<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Meja extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('M_meja');
	}

	public function index()
	{
		$data['meja'] = $this->M_meja->list_meja('meja')->result();

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/master/meja/index',$data);
		$this->load->view('dashboard/footer');		
	}

	function tambah(){
		$data = array('nama_meja' => $this->input->post('meja'));
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil ditambahkan",
		);

		$this->M_meja->create($data,'meja');
		$this->session->set_flashdata($notif);
		redirect('Admin/Meja');
	}

	function edit(){
		$id = $this->input->post('id');

		$result = $this->M_meja->get($id,'meja');
		echo json_encode($result);

	}

	function update(){
		$where = array('id_meja' => $this->input->post('id'));
		$data = array('nama_meja' => $this->input->post('meja'));
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil diupdate",
		);

		$this->M_meja->replace($where,$data,'meja');
		$this->session->set_flashdata($notif);
		redirect('Admin/Meja');
	}

	function hapus($id){
		$where = array('id_meja' => $id);
		
		$this->M_meja->trash($where,'meja');
		redirect('Admin/Meja');
	}

}

/* End of file Meja.php */
/* Location: ./application/controllers/Meja.php */