<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_laporan');

		$this->load->library('pdf');
	}

	public function index()
	{
		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/laporan');
		$this->load->view('dashboard/footer');
	}

	public function cetak_jual_item(){
		$where = array('tgl_order' => $this->input->post('tanggal'));

	    $pdf = new FPDF('l','mm','A4');
	    $pdf->SetTitle('Laporan Penjualan Item (Harian)');
	        
	    $pdf->AliasNbPages();
	    $pdf->AddPage();
	    //$pdf->Image('assets/images/logo.png',10,10,50,50);
	    // setting jenis font yang akan digunakan
	    $pdf->SetFont('Arial','B',16);
	    // mencetak string 
	    // $pdf->Image('assets/images/logo.png',20,10,20,20);
	    $pdf->Cell(280,7,'Daftar Penjualan Per Item Harian',0,1,'C');
	    $pdf->SetFont('Arial','',12);
	    $pdf->Cell(280,7,'Tanggal : '.date('d M Y',strtotime($this->input->post('tanggal'))),0,1,'C');
	    $pdf->SetFont('Arial','B',12);
	    // Memberikan space kebawah agar tidak terlalu rapat
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->SetFillColor(17,56,256);
	    $pdf->SetFont('Arial','B',10);
		$pdf->Cell(30,7,'',0,0);
	    $pdf->Cell(100,6,'Nama Produk',1,0,'C',TRUE);
	    $pdf->Cell(35,6,'Harga Produk',1,0,'C',TRUE);
	    $pdf->Cell(27,6,'Jumlah',1,0,'C',TRUE);
	    $pdf->Cell(50,6,'Total',1,1,'C',TRUE);
	    $pdf->SetFont('Arial','',10);
	        
	    $produk = $this->M_laporan->list_sell_item($where,'pesanan')->result();
	    // var_dump($produk);
	    foreach ($produk as $row){
	        $pdf->Cell(30,7,'',0,0);
	        $pdf->Cell(100,6,$row->nama_produk,1,0);
	        $pdf->Cell(35,6,"Rp ".number_format($row->harga_produk,'0',',','.'),1,0);
	        $pdf->Cell(27,6,$row->qty,1,0,'C');
	        $pdf->Cell(50,6,"Rp ".number_format($row->harga_produk * $row->qty,'0',',','.'),1,1);
	    }

	    $harga = $this->M_laporan->get_harga($where,'pesanan')->result();

	    foreach($harga as $h){
	    	$a[] = $h->sub_harga;
	    }

	    $total = array_sum($a);

	    $pdf->Cell(30,6,'',0,0);
	    $pdf->Cell(162,6,'Total',1,0,'C');
	    $pdf->Cell(50,6,"Rp ".number_format($total,'0',',','.'),1,1);

	    // Go to 1.5 cm from bottom
	    // $pdf->SetY(20);
	    // $pdf->SetFont('Arial','I',8);
	    // $pdf->Cell(0,10,'Page '.$pdf->PageNo().' of {nb} '.' '.' | '.' '.' Uma Menuh',0,0,'C');

	    $pdf->Output();
	}

	public function cetak_jual_item_bulan(){
		$where = $this->input->post('bulan');

	    $pdf = new FPDF('l','mm','A4');
	    $pdf->SetTitle('Laporan Penjualan Item (Bulanan)');
	        
	    $pdf->AliasNbPages();
	    $pdf->AddPage();
	    //$pdf->Image('assets/images/logo.png',10,10,50,50);
	    // setting jenis font yang akan digunakan
	    $pdf->SetFont('Arial','B',16);
	    // mencetak string 
	    // $pdf->Image('assets/images/logo.png',20,10,20,20);
	    $pdf->Cell(280,7,'Daftar Penjualan Per Item Bulanan',0,1,'C');
	    $pdf->SetFont('Arial','',12);
	    $bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');

	    $pdf->Cell(280,7,'Bulan '.$bulan[$where],0,1,'C');
	    $pdf->SetFont('Arial','B',12);
	    // Memberikan space kebawah agar tidak terlalu rapat
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->SetFillColor(17,56,256);
	    $pdf->SetFont('Arial','B',10);
		$pdf->Cell(30,7,'',0,0);
	    $pdf->Cell(100,6,'Nama Produk',1,0,'C',TRUE);
	    $pdf->Cell(35,6,'Harga Produk',1,0,'C',TRUE);
	    $pdf->Cell(27,6,'Jumlah',1,0,'C',TRUE);
	    $pdf->Cell(50,6,'Total',1,1,'C',TRUE);
	    $pdf->SetFont('Arial','',10);
	        
	    $produk = $this->M_laporan->list_sell_item_bulan($where,'pesanan')->result();
	    // var_dump($produk);
	    foreach ($produk as $row){
	        $pdf->Cell(30,7,'',0,0);
	        $pdf->Cell(100,6,$row->nama_produk,1,0);
	        $pdf->Cell(35,6,"Rp ".number_format($row->harga_produk,'0',',','.'),1,0);
	        $pdf->Cell(27,6,$row->qty,1,0,'C');
	        $pdf->Cell(50,6,"Rp ".number_format($row->harga_produk * $row->qty,'0',',','.'),1,1);
	    }

	    $harga = $this->M_laporan->get_harga_bulan($where,'pesanan')->result();

	    foreach($harga as $h){
	    	$a[] = $h->sub_harga;
	    }

	    $total = array_sum($a);

	    $pdf->Cell(30,6,'',0,0);
	    $pdf->Cell(162,6,'Total',1,0,'C');
	    $pdf->Cell(50,6,"Rp ".number_format($total,'0',',','.'),1,1);

	    // Go to 1.5 cm from bottom
	    // $pdf->SetY(20);
	    // $pdf->SetFont('Arial','I',8);
	    // $pdf->Cell(0,10,'Page '.$pdf->PageNo().' of {nb} '.' '.' | '.' '.' Uma Menuh',0,0,'C');

	    $pdf->Output();
	}

	public function cetak_jual_kotor(){
		$where = array('tgl_order' => $this->input->post('tanggal'));

	    $pdf = new FPDF('l','mm','A4');
	    $pdf->SetTitle('Laporan Penpadatan (Harian)');
	        
	    $pdf->AliasNbPages();
	    $pdf->AddPage();
	    //$pdf->Image('assets/images/logo.png',10,10,50,50);
	    // setting jenis font yang akan digunakan
	    $pdf->SetFont('Arial','B',16);
	    // mencetak string 
	    // $pdf->Image('assets/images/logo.png',20,10,20,20);
	    $pdf->Cell(280,7,'Laporan Pendapatan Harian',0,1,'C');
	    $pdf->SetFont('Arial','B',12);
	    // Memberikan space kebawah agar tidak terlalu rapat
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->SetFillColor(17,56,256);
	    $pdf->SetFont('Arial','B',10);
		$pdf->Cell(20,7,'',0,0);
	    $pdf->Cell(50,6,'No. Pesanan',1,0,'C',TRUE);
	    $pdf->Cell(25,6,'Total (K)',1,0,'C',TRUE);
	    $pdf->Cell(25,6,'Total (B)',1,0,'C',TRUE);
	    $pdf->Cell(35,6,'B. Kartu (D)',1,0,'C',TRUE);
	    $pdf->Cell(35,6,'B. Kartu (K)',1,0,'C',TRUE);
	    $pdf->Cell(27,6,'Komisi',1,0,'C',TRUE);
	    $pdf->Cell(50,6,'Tanggal',1,1,'C',TRUE);
	    $pdf->SetFont('Arial','',10);
	        
	    $produk = $this->M_laporan->list_sell_dirty($where,'pesanan')->result();
	    // var_dump($produk);
	    foreach ($produk as $row){
	        $pdf->Cell(20,7,'',0,0);
	        $pdf->Cell(50,6,$row->id_order,1,0);
	        $pdf->Cell(25,6,"Rp ".number_format($row->total_kotor,'0',',','.'),1,0);
	        $pdf->Cell(25,6,"Rp ".number_format($row->total_bersih,'0',',','.'),1,0);
	        $pdf->Cell(35,6,"Rp ".number_format($row->bayar_debit,'0',',','.'),1,0);
	        $pdf->Cell(35,6,"Rp ".number_format($row->bayar_kredit,'0',',','.'),1,0);
	        $pdf->Cell(27,6,"Rp ".number_format($row->komisi,'0',',','.'),1,0);
	        $pdf->Cell(50,6,$row->tgl_order,1,1,'C');
	    }

	    $kotor = $this->M_laporan->get_kotor($where,'pesanan')->result();

	    foreach($kotor as $k){
	    	$a[] = $k->total_kotor;
	    	$b[] = $k->total_bersih;
	    	$c[] = $k->komisi;
	    }

	    $totalk = array_sum($a);
	    $totalb = array_sum($b);
	    $totalko = array_sum($c);

	    $pdf->Cell(30,6,'',0,1);
	    $pdf->Cell(138,6,'',0,0);
	    $pdf->Cell(30,6,'',0,0);
	    $pdf->Cell(50,6,'Total Pendapatan Kotor :',0,0);
	    $pdf->Cell(25,6,"Rp ".number_format($totalk,'0',',','.'),0,1);
	    $pdf->Cell(138,6,'',0,0);
	    $pdf->Cell(30,6,'',0,0);
	    $pdf->Cell(50,6,'Total Pendapatan Bersih :',0,0);
	    $pdf->Cell(25,6,"Rp ".number_format($totalb,'0',',','.'),0,1);
	    $pdf->Cell(138,6,'',0,0);
	    $pdf->Cell(30,6,'',0,0);
	    $pdf->Cell(50,6,'Total Komisi Keluar :',0,0);
	    $pdf->Cell(25,6,'Rp '.number_format($totalko,'0',',','.'),0,1);
	    $pdf->Cell(138,6,'',0,0);
	    $pdf->Cell(30,6,'',0,0);
	    $pdf->Cell(50,6,'Per Tanggal :',0,0);
	    $pdf->Cell(25,6,date('d F Y',strtotime($this->input->post('tanggal'))),0,1);

	    // Go to 1.5 cm from bottom
	    // $pdf->SetY(20);
	    // $pdf->SetFont('Arial','I',8);
	    // $pdf->Cell(0,10,'Page '.$pdf->PageNo().' of {nb} '.' '.' | '.' '.' Uma Menuh',0,0,'C');

	    $pdf->Output();
	}

	public function cetak_jual_kotor_bulan(){
		$where = $this->input->post('bulan');

	    $pdf = new FPDF('l','mm','A4');
	    $pdf->SetTitle('Laporan Penpadatan (Bulanan)');
	        
	    $pdf->AliasNbPages();
	    $pdf->AddPage();
	    //$pdf->Image('assets/images/logo.png',10,10,50,50);
	    // setting jenis font yang akan digunakan
	    $pdf->SetFont('Arial','B',16);
	    // mencetak string 
	    // $pdf->Image('assets/images/logo.png',20,10,20,20);
	    $pdf->Cell(280,7,'Laporan Pendapatan Bulanan',0,1,'C');
	    $pdf->SetFont('Arial','B',12);
	    // Memberikan space kebawah agar tidak terlalu rapat
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->Cell(10,7,'',0,1);
	    $pdf->SetFillColor(17,56,256);
	    $pdf->SetFont('Arial','B',10);
		$pdf->Cell(20,7,'',0,0);
	    $pdf->Cell(50,6,'No. Pesanan',1,0,'C',TRUE);
	    $pdf->Cell(25,6,'Total (K)',1,0,'C',TRUE);
	    $pdf->Cell(25,6,'Total (B)',1,0,'C',TRUE);
	    $pdf->Cell(35,6,'B. Kartu (D)',1,0,'C',TRUE);
	    $pdf->Cell(35,6,'B. Kartu (K)',1,0,'C',TRUE);
	    $pdf->Cell(27,6,'Komisi',1,0,'C',TRUE);
	    $pdf->Cell(50,6,'Tanggal',1,1,'C',TRUE);
	    $pdf->SetFont('Arial','',10);
	        
	    $produk = $this->M_laporan->list_sell_dirty_bulan($where,'pesanan')->result();
	    // var_dump($produk);
	    foreach ($produk as $row){
	        $pdf->Cell(20,7,'',0,0);
	        $pdf->Cell(50,6,$row->id_order,1,0);
	        $pdf->Cell(25,6,"Rp ".number_format($row->total_kotor,'0',',','.'),1,0);
	        $pdf->Cell(25,6,"Rp ".number_format($row->total_bersih,'0',',','.'),1,0);
	        $pdf->Cell(35,6,"Rp ".number_format($row->bayar_debit,'0',',','.'),1,0);
	        $pdf->Cell(35,6,"Rp ".number_format($row->bayar_kredit,'0',',','.'),1,0);
	        $pdf->Cell(27,6,"Rp ".number_format($row->komisi,'0',',','.'),1,0);
	        $pdf->Cell(50,6,$row->tgl_order,1,1,'C');
	    }

	    $kotor = $this->M_laporan->get_kotor_bulan($where,'pesanan')->result();

	    foreach($kotor as $k){
	    	$a[] = $k->total_kotor;
	    	$b[] = $k->total_bersih;
	    	$c[] = $k->komisi;
	    }

	    $totalk = array_sum($a);
	    $totalb = array_sum($b);
	    $totalko = array_sum($c);

	    $pdf->Cell(30,6,'',0,1);
	    $pdf->Cell(138,6,'',0,0);
	    $pdf->Cell(30,6,'',0,0);
	    $pdf->Cell(50,6,'Total Pendapatan Kotor :',0,0);
	    $pdf->Cell(25,6,"Rp ".number_format($totalk,'0',',','.'),0,1);
	    $pdf->Cell(138,6,'',0,0);
	    $pdf->Cell(30,6,'',0,0);
	    $pdf->Cell(50,6,'Total Pendapatan Bersih :',0,0);
	    $pdf->Cell(25,6,"Rp ".number_format($totalb,'0',',','.'),0,1);
	    $pdf->Cell(138,6,'',0,0);
	    $pdf->Cell(30,6,'',0,0);
	    $pdf->Cell(50,6,'Total Komisi Keluar :',0,0);
	    $pdf->Cell(25,6,'Rp '.number_format($totalko,'0',',','.'),0,1);
	    $pdf->Cell(138,6,'',0,0);
	    $pdf->Cell(30,6,'',0,0);
	    $pdf->Cell(50,6,'Per Bulan :',0,0);
	    $bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
	    $pdf->Cell(25,6,$bulan[$where],0,1);

	    // Go to 1.5 cm from bottom
	    // $pdf->SetY(20);
	    // $pdf->SetFont('Arial','I',8);
	    // $pdf->Cell(0,10,'Page '.$pdf->PageNo().' of {nb} '.' '.' | '.' '.' Uma Menuh',0,0,'C');

	    $pdf->Output();
	}

}

/* End of file Laporan.php */
/* Location: ./application/controllers/Laporan.php */