<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Minuman extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('M_minuman');
	}

	public function index()
	{
		$data['produk'] = $this->M_minuman->list_minuman('produk')->result();

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/produk/minuman/index',$data);
		$this->load->view('dashboard/footer');
	}

	function tambah(){
		$data = array(
			'nama_produk' => $this->input->post('minuman'),
			'detail_produk' => $this->input->post('detail'),
			'harga_produk' => $this->input->post('harga'),
			'tipe_produk' => "mnm",
		);
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil ditambahkan",
		);

		$this->M_minuman->create($data,'produk');
		$this->session->set_flashdata($notif);
		redirect('Admin/Minuman');
	}

	function edit($id){
		$where = array('id_produk' => $id);
		$data['produk'] = $this->M_minuman->get($where,'produk');

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/produk/minuman/edit',$data);
		$this->load->view('dashboard/footer');
	}

	function update(){
		$where = array('id_produk' => $this->input->post('id'));
		$data = array(
			'nama_produk' => $this->input->post('minuman'),
			'detail_produk' => $this->input->post('detail'),
			'harga_produk' => $this->input->post('harga'),
		);
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil diupdate",
		);

		$this->M_minuman->replace($where,$data,'produk');
		$this->session->set_flashdata($notif);
		redirect('Admin/Minuman');
	}

	function hapus($id){
		$where = array('id_produk' => $id);

		$this->M_minuman->trash($where,'produk');
		redirect('Admin/Minuman');
	}

}

/* End of file Minuman.php */
/* Location: ./application/controllers/Minuman.php */