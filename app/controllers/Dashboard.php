<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('logged_in') != TRUE){
			$notif = array(
				'status' => "gagal",
				'message' => "Silahkan login terlebih dahulu",
			);

			$this->session->set_flashdata($notif);
			redirect('login');
		}
		$this->load->model('M_kasir');
		$this->load->model('M_meja');
	}

	public function index()
	{
		$data['produk'] = $this->M_kasir->list_produk('produk')->result();
		$data['meja'] = $this->M_meja->list_meja('meja')->result();
		$data['order'] = $this->M_kasir->list_order('order_temp')->result();
		$data['kode'] = $this->M_kasir->code_otomatis();

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/index',$data);
		$this->load->view('dashboard/footer');
	}

	public function bill(){
		$data['tagihan'] = $this->M_kasir->list_tagihan('pesanan')->result();

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/kasir/bill/index',$data);
		$this->load->view('dashboard/footer');
	}

	public function bill_detail($id){
		$this->load->model('M_bank');
		$where = array('id_order' => $id);
		$data = array(
			'detail' => $this->M_kasir->get_bill($where,'pesanan'),
			'bank' => $this->M_bank->list_bank('bank')->result(),
		);

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/kasir/bill/detail',$data);
		$this->load->view('dashboard/footer');
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */