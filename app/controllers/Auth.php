<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_auth');
	}

	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');	

		$cek = $this->M_auth->cek_user($username,'user');

		if($cek->num_rows() > 0){
			$s = $cek->row();
			if(password_verify($password,$s->password)){
				$session = array(
					'id' => $s->id_user,
					'nama' => $s->nama_user,
					'jk' => $s->jk,
					'no_hp' => $s->no_hp,
					'alamat' => $s->alamat,
					'email' => $s->email,
					'username' => $s->username,
					'path' => $s->path,
					'akses' => $s->akses,
					'logged_in' => TRUE,
				);

				$this->session->set_userdata($session);
				redirect('Dashboard');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf Password salah, silahkan cek kembali",
				);

				$this->session->set_flashdata($notif);
				redirect('login');
			}
		}else{
			$notif = array(
				'status' => "gagal",
				'message' => "Maaf User ".$username." Tidak terdaftar dalam sistem",
			);

			$this->session->set_flashdata($notif);
			redirect('login');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */