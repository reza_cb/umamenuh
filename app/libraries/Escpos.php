<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// IMPORTANT - Replace the following line with your path to the escpos-php autoload script
require_once __DIR__ . '\..\third_party\autoload.php';
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;
class Escpos {
  private $CI;
  private $connector;
  public $printer;
  // TODO: printer settings
  // Make this configurable by printer (32 or 48 probably)
  public $printer_width = 32;
  public function __construct()
  {
    $this->CI =& get_instance(); // This allows you to call models or other CI objects with $this->CI->... 
  }

  function connect($name)
  {
    $this->connector = new WindowsPrintConnector($name);
    $this->printer = new Printer($this->connector);
  }

  public function check_connection()
  {
    if (!$this->connector OR !$this->printer OR !is_a($this->printer, 'Mike42\Escpos\Printer')) {
      throw new Exception("Tried to create receipt without being connected to a printer.");
    }
  }

  public function close_after_exception()
  {
    if (isset($this->printer) && is_a($this->printer, 'Mike42\Escpos\Printer')) {
      $this->printer->close();
    }
    $this->connector = null;
    $this->printer = null;
    $this->emc_printer = null;
  }

  // Calls printer->text and adds new line
  public function add_line($text = "", $should_wordwrap = true)
  {
    $text = $should_wordwrap ? wordwrap($text, $this->printer_width) : $text;
    $this->printer->text($text."\n");
  }

  public function print_test_bill($text = "")
  {
    $this->CI->load->model('M_kasir');
    $this->check_connection();
    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->add_line("Bebek");
    $this->add_line("Uma Menuh");
    $this->printer->selectPrintMode();
    $this->add_line("Kemenuh, Kecamatan Sukawati, Kabupaten Gianyar, Bali");
    $this->add_line($text['tgl_order']);
    $this->add_line("============================");
    $this->printer->close();

    $this->printer->setJustification(Printer::JUSTIFY_LEFT);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    $this->add_line(); // blank line
    $this->add_line("No. Pesanan : ".$text['id_order']);
    $this->add_line("No. Meja : ".$text['nama_meja']);
    // $this->add_line("Kode : ".$text['kodeunik']);
    $this->add_line();

    $where = array('id_order' => $text['id_order']);
    $detail['detail'] = $this->CI->M_kasir->ambil_detail($where,'detail_order')->result();

    foreach($detail['detail'] as $d){
      $this->add_line($d->nama_produk."\n   ".$d->qty."x".number_format($d->harga_produk,'0',',','.')." = Rp ".number_format($d->sub_harga,'0',',','.'));
    }
    
    $this->add_line();
    $this->add_line("Tax : 11%");
    $this->add_line("Service : 10%");
    $this->add_line("---------------------------+");
    $this->add_line("Total : Rp ".number_format($text['total_kotor'],'0',',','.'));
    $this->add_line(); // blank line
    $this->printer->close();

    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    $this->add_line("============================");
    $this->add_line("Thank You For Visit");
    $this->add_line("See You Soon");
    $this->printer->cut(Printer::CUT_PARTIAL);
    $this->printer->close();
  }

  public function print_test_receipt($text = "")
  {
    $this->CI->load->model('M_kasir');
    $this->check_connection();
    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->add_line("Bebek");
    $this->add_line("Uma Menuh");
    $this->printer->selectPrintMode();
    $this->add_line("Kemenuh, Kecamatan Sukawati, Kabupaten Gianyar, Bali");
    $this->add_line($text['tgl_order']);
    $this->add_line("============================");
    $this->printer->close();

    $this->printer->setJustification(Printer::JUSTIFY_LEFT);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    $this->add_line(); // blank line
    $this->add_line("No. Pesanan : ".$text['id_order']);
    $this->add_line("No. Meja : ".$text['nama_meja']);
    // $this->add_line("Kode : ".$text['kodeunik']);
    $this->add_line();

    $where = array('id_order' => $text['id_order']);
    $detail['detail'] = $this->CI->M_kasir->ambil_detail($where,'detail_order')->result();

    foreach($detail['detail'] as $d){
      $this->add_line($d->nama_produk."\n   ".$d->qty."x".number_format($d->harga_produk,'0',',','.')." = Rp ".number_format($d->sub_harga,'0',',','.'));
    }
    
    $this->add_line();
    $this->add_line("Tax : 11%");
    $this->add_line("Service : 10%");
    $this->add_line("---------------------------+");
    $this->add_line("Total : Rp ".number_format($text['total_kotor'],'0',',','.'));
    $this->add_line("Bayar : Rp ".number_format($text['bayar'],'0',',','.'));
    $this->add_line("Kembalian : Rp ".number_format($text['kembalian'],'0',',','.'));
    $this->add_line(); // blank line
    $this->printer->close();

    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    $this->add_line("============================");
    $this->add_line("Thank You For Visit");
    $this->add_line("See You Soon");
    $this->printer->cut(Printer::CUT_PARTIAL);
    $this->printer->close();
  }

  public function print_kitchen($text = "")
  {
    $this->CI->load->model('M_kasir');
    $this->check_connection();
    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->add_line("Catatan");
    $this->add_line("Pesanan");
    $this->printer->selectPrintMode();
    $this->add_line($text['tgl_order']);
    // $this->add_line("============================");
    $this->printer->close();

    $this->printer->setJustification(Printer::JUSTIFY_LEFT);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    $this->add_line(); // blank line
    $this->add_line("No. Meja : ".$text['nama_meja']);
    // $this->add_line("Kode : ".$text['kodeunik']);
    $this->add_line("============================");

    $where = array('id_order' => $text['id_order']);
    $detail['detail'] = $this->CI->M_kasir->ambil_detail($where,'detail_order')->result();

    foreach($detail['detail'] as $d){
      $this->add_line($d->nama_produk." : ".$d->qty."\n".$d->catatan."\n");
    }
    
    $this->add_line("============================="); // blank line
    $this->printer->close();

    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    // $this->add_line("============================");
    $this->printer->cut(Printer::CUT_PARTIAL);
    $this->printer->close();
  }

  public function print_test_guide($text = "")
  {
    $this->CI->load->model('M_kasir');
    $this->check_connection();
    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->add_line("Bebek");
    $this->add_line("Uma Menuh");
    $this->printer->selectPrintMode();
    $this->add_line("Kemenuh, Kecamatan Sukawati, Kabupaten Gianyar, Bali");
    $this->add_line($text['tgl_order']);
    $this->add_line("============================");
    $this->printer->close();

    $this->printer->setJustification(Printer::JUSTIFY_LEFT);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    $this->add_line(); // blank line
    $this->add_line("No. Pesanan : ".$text['id_order']);
    $this->add_line("No. Meja : ".$text['nama_meja']);
    $this->add_line("Kode : ".$text['kodeunik']);
    $this->add_line();

    $where = array('id_order' => $text['id_order']);
    $detail['detail'] = $this->CI->M_kasir->ambil_detail_guide($where,'detail_order')->result();

    foreach($detail['detail'] as $d){
      $this->add_line($d->nama_produk."\n   ".$d->qty."x".number_format($d->harga_produk,'0',',','.')." = Rp ".number_format($d->sub_harga,'0',',','.'));
    }
    
    $this->add_line();
    $this->add_line("Komisi : 35%");
    $this->add_line("---------------------------+");
    $this->add_line("Komisi : Rp ".number_format($text['komisi'],'0',',','.'));
    $this->add_line(); // blank line
    $this->printer->close();

    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    $this->add_line("============================");
    $this->add_line("Thank You For Visit");
    $this->add_line("See You Soon");
    $this->printer->cut(Printer::CUT_PARTIAL);
    $this->printer->close();
  }

  public function print_test_guide_new($text = "")
  {
    $this->CI->load->model('M_kasir');
    $this->check_connection();
    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->add_line("Bebek");
    $this->add_line("Uma Menuh");
    $this->printer->selectPrintMode();
    $this->add_line("Kemenuh, Kecamatan Sukawati, Kabupaten Gianyar, Bali");
    $this->add_line($text['tgl_order']);
    $this->add_line("============================");
    $this->printer->close();

    $this->printer->setJustification(Printer::JUSTIFY_LEFT);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    $this->add_line(); // blank line
    $this->add_line("No. Pesanan : ".$text['id_order']);
    $this->add_line("No. Meja : ".$text['nama_meja']);
    $this->add_line("Kode : ".$text['kodeunik']);
    $this->add_line();

    // $where = array('id_order' => $text['id_order']);
    $detail['detail'] = $this->CI->M_kasir->ambil_detail_guide_new('order_temp')->result();

    foreach($detail['detail'] as $d){
      $this->add_line($d->nama_produk."\n   ".$d->qty."x".number_format($d->harga_produk,'0',',','.')." = Rp ".number_format($d->subharga,'0',',','.'));
    }

    foreach($detail['detail'] as $k){
      $harga[] = $k->subharga;
    }

    $totalmkn = array_sum($harga);
    $hitungkomisi = $totalmkn * 0.35;
    
    $this->add_line();
    $this->add_line("Komisi : 35%");
    $this->add_line("---------------------------+");
    $this->add_line("Komisi : Rp ".number_format($hitungkomisi,'0',',','.'));
    $this->add_line(); // blank line
    $this->printer->close();

    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    $this->add_line("============================");
    $this->add_line("Thank You For Visit");
    $this->add_line("See You Soon");
    $this->printer->cut(Printer::CUT_PARTIAL);
    $this->printer->close();
  }
}
